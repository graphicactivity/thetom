<?php

return array(
    '*' => array(
        'tablePrefix' => 'craft',
        'server' => 'localhost',
    ),
    'thetom.dev' => array(
    	'database' => 'database',
        'user' => 'user',
        'password' => 'password',
    ),
    'thetom.co.nz' => array(
    	'database' => 'database',
        'user' => 'user',
        'password' => 'password',
    ),
);
