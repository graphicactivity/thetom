<?php

return array(
    '*' => array(
        'siteUrl' => null,
        'defaultWeekStartDay' => 0,
        'enableCsrfProtection' => true,
        'omitScriptNameInUrls' => true,
        'cpTrigger' => 'admin',
    ),

    'thetom.dev' => array(
        'devMode' => true,
        'environmentVariables' => array(
            'basePath' => '/Users/GraphicActivity/Sites/thetom.dev/public/',
            'baseUrl'  => 'http://thetom.dev/',
        )
    ),

    'thetom.co.nz' => array(
        'devMode' => false,
        'environmentVariables' => array(
            'basePath' => '/var/www/html/',
            'baseUrl'  => 'https://thetom.co.nz/',
        )
    )
);
